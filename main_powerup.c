/***************************************************************************
/ Power Up + Frame Capture + Advanced Algorithm
***************************************************************************/ 
#include <string.h>

#include "nrf_drv_spi.h"
#include "app_util_platform.h"
#include "nrf_gpio.h"
#include "nrf_delay.h"
#include "boards.h"
#include "app_error.h"
#include "nrf_log.h"
#include "nrf_log_ctrl.h"
#include "nrf_log_default_backends.h"

#include "registros.h"

#define   COMMAND_SIZE  2         /**< Size for normal buffer */

static const nrf_drv_spi_t spi = NRF_DRV_SPI_INSTANCE(SPI_INSTANCE);  /**< SPI instance. */
static volatile bool spi_xfer_done;  /**< Flag used to indicate that SPI instance completed the transfer. */
static volatile bool config_done = false;
static uint8_t SW_SMART_Flag = 0x00; 
                                          
static uint8_t m_tx_buf[COMMAND_SIZE];                          /**< TX buffer.       */
static uint8_t m_rx_buf[COMMAND_SIZE + 1];                       /**< RX buffer */
static const uint8_t m_tx_length = sizeof(m_tx_buf);            /**< Transfer length. */
static const uint8_t m_rx_length = sizeof(m_rx_buf);            /**< Receiver length. */

static uint8_t m_rburst_buf[sizeof(MOTION_registers) + 1];      /**< RX buffer for burst read. */
static const uint8_t m_burst_length = sizeof(m_rburst_buf);     /**< Length for burst buffer */

static uint8_t command_data[COMMAND_SIZE];                      /**< TX buffer for commands transmission */
//static uint8_t command_data_rx[sizeof(command_data) + 1];       /**< TX buffer for commands data reception */

void spi_event_handler(nrf_drv_spi_evt_t const * p_event, void * p_context)
{
    spi_xfer_done = true;
    NRF_LOG_INFO("Transfer completed.");

    if (m_rx_buf[0] != 0){
        NRF_LOG_INFO(" Received:");
        NRF_LOG_HEXDUMP_INFO(m_rx_buf, strlen((const char *)m_rx_buf));
    }
    if (m_rburst_buf[0] != 0){
        NRF_LOG_INFO(" Received:");
        NRF_LOG_HEXDUMP_INFO(m_rburst_buf, strlen((const char *)m_rburst_buf));
    }
}

void read_operation(uint8_t addr){
    bool big_buf = false;   /**< Flag for using a normal or big buffer */
    if(addr == BURST_READ)
        big_buf = true;

    int mask = 1<<7;
    addr = addr & ~mask; /* Set MSB bit to 0 */

    m_tx_buf[0]= addr;
    m_tx_buf[1]= 0x00;
  
    // Reset transfer done flag
    spi_xfer_done = false;
    if(!big_buf){
        // Reset memory
        memset(m_rx_buf, 0, m_rx_length);

        APP_ERROR_CHECK(nrf_drv_spi_transfer(&spi, m_tx_buf, m_tx_length, m_rx_buf, m_rx_length));
        while (!spi_xfer_done){
            __WFE();
        }
    }
    else{
        // Reset memory
        memset(m_rburst_buf, 0, m_burst_length);

        APP_ERROR_CHECK(nrf_drv_spi_transfer(&spi, m_tx_buf, m_tx_length, m_rburst_buf, m_burst_length));
        while (!spi_xfer_done){
            __WFE();
        }
    }
    

    NRF_LOG_FLUSH();
}


void write_operation(uint8_t addr, uint8_t data){
    static uint8_t       m_tx_write[COMMAND_SIZE];
    static const uint8_t m_length = sizeof(m_tx_write);        /**< Write Transfer length. */

    int mask = 1<<7;
    addr = addr|mask; /* Set MSB bit to 1*/
    
    m_tx_write[0]= addr;
    m_tx_write[1]= data;           /**< Write buffer. */
    

    // Reset transfer done flag
    spi_xfer_done = false;

    command_data[0]= SPI_CLK_ON_REQ;
    command_data[1]= CLK_ON; 
    APP_ERROR_CHECK(nrf_drv_spi_transfer(&spi,command_data,sizeof(command_data), NULL, 0));
    nrf_delay_us(300);
    NRF_LOG_INFO("Transmitido CLK_ON:");
    
    spi_xfer_done = false;
    APP_ERROR_CHECK(nrf_drv_spi_transfer(&spi, m_tx_write, m_length,NULL, 0));
    while (!spi_xfer_done){
       __WFE();
    }
    NRF_LOG_INFO("Transmitido Datos a escribir:");

    spi_xfer_done = false;
    command_data[0]= SPI_CLK_ON_REQ;
    command_data[1]= CLK_OFF; 
    APP_ERROR_CHECK(nrf_drv_spi_transfer(&spi,command_data,sizeof(command_data), NULL, 0));
    while (!spi_xfer_done){
        __WFE();
    }
    NRF_LOG_INFO("Transmitido CLK_OFF:");

    NRF_LOG_FLUSH();
}


int main(void){

    bool verified = false;
    bsp_board_init(BSP_INIT_LEDS);

    APP_ERROR_CHECK(NRF_LOG_INIT(NULL));
    NRF_LOG_DEFAULT_BACKENDS_INIT();

    nrf_drv_spi_config_t spi_config = NRF_DRV_SPI_DEFAULT_CONFIG;
    spi_config.ss_pin   = SPI_SS_PIN;
    spi_config.miso_pin = SPI_MISO_PIN;
    spi_config.mosi_pin = SPI_MOSI_PIN;
    spi_config.sck_pin  = SPI_SCK_PIN;

    APP_ERROR_CHECK(nrf_drv_spi_init(&spi, &spi_config, spi_event_handler, NULL));

    NRF_LOG_INFO("SPI+Laser example started.");


    /*
    
    //Self TEST 
    //Expected results after self test procedure completed,
    //CRC0 = 0xb4
    //CRC1 = 0xf3
    //CRC2 = 0x15
    //CRC3 = 0x1b

    write_operation(POWER_UP_RESET, RESET);
    nrf_delay_ms(25);
    write_operation(DTEST2_PAD, TEST_CLK_ON);
    write_operation(SELF_TEST, 0x01);
    nrf_delay_ms(512);
    read_operation(CRC0); 
    nrf_delay_us(1);
    read_operation(CRC1);
    nrf_delay_us(1);
    read_operation(CRC2);
    nrf_delay_us(1);
    read_operation(CRC3);

    write_operation(POWER_UP_RESET, RESET);
    
    */



    /* Power up */
    write_operation(POWER_UP_RESET, WAKE_UP); /*0x96 to wakeup from shutdown, 0x05 to reset the chip*/
    nrf_delay_us(30);
    do{
        write_operation(OBSERVATION1, 0x00);
        nrf_delay_ms(10);
        read_operation(OBSERVATION1);
        nrf_delay_us(1);
    }
    while((m_rburst_buf[0] & 0x0F) != 0x0F);
    read_operation(MOTION);
    nrf_delay_us(1);
    read_operation(DELTA_X_L);
    nrf_delay_us(1);
    read_operation(DELTA_Y_L);
    nrf_delay_us(1);
    read_operation(DELTA_XY_H);
    nrf_delay_us(1);
    write_operation(PERFORMANCE, 0x0d);
    nrf_delay_us(30);
    write_operation(RUN_DOWNSHIFT,0x04);
    nrf_delay_us(30);
    write_operation(REST1_RATE,0x04);
    nrf_delay_us(30);
    write_operation(REST1_DOWNSHIFT,0x0f);
    nrf_delay_us(30);



    /* Frame capture */
    write_operation(PERFORMANCE, DIS_REST_MODE);
    NRF_LOG_INFO("Transmitido DIS_REST_MODE");
    nrf_delay_us(30);
    write_operation(DTEST2_PAD, TEST_CLK_ON);
    NRF_LOG_INFO("Transmitido TEST_CLK_ON:");
    nrf_delay_us(30);
    write_operation(FRAME_GRAB, EN_FRAME_GR);
    NRF_LOG_INFO("Transmitido EN_FRAME_GRAB:");
    nrf_delay_ms(10);

    read_operation(BURST_READ);

    /*Algorithm to enhance surface coverage*/
    if(SW_SMART_Flag && m_rx_buf[5] == 0 && m_rx_buf[6]<45){    
        write_operation(DTEST2_PAD, 0x00); /* Smart enable*/
        NRF_LOG_INFO("Transmitido valor a TEST_PAD");
            
        SW_SMART_Flag = 0;
    }
    else if(!SW_SMART_Flag && m_rx_buf[5] == 0 && m_rx_buf[6]>=45){    
        write_operation(DTEST2_PAD, 0x80); /* Smart disable*/
        NRF_LOG_INFO("Transmitido valor a TEST_PAD");

        SW_SMART_Flag = 1;
    }




    /* Shutdown */
    write_operation(SHUTDOWN_REG, SHUTDOWN);



    NRF_LOG_FLUSH();

    while(1){
        bsp_board_led_invert(BSP_BOARD_LED_0);
        nrf_delay_ms(1000);
    }
}

