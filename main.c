/* Slave select is handled by the driver, i.e. SPI_CS_PIN is 
set low right before the transfer starts and set back high 
when transfer is done.
*/
#include <string.h>

#include "nrf_drv_spi.h"
#include "app_util_platform.h"
#include "nrf_gpio.h"
#include "nrf_delay.h"
#include "boards.h"
#include "app_error.h"
#include "nrf_log.h"
#include "nrf_log_ctrl.h"
#include "nrf_log_default_backends.h"

#define SPI_INSTANCE    0       /**< SPI instance index. */
#define MOTION_registers 10     /**< Number of registers to read */

#define MOTION          0x02    /**< SPI Motion register address */
#define DELTA_X_L       0x03    /**< SPI DELTA_X_L register address */
#define DELTA_Y_L       0x04    /**< SPI DELTA_Y_L register address */
#define DELTA_XY_H      0x05    /**< SPI DELTA_XY_H register address */
#define BURST_READ      0x12    /**< SPI Motion Burst register address */

static const nrf_drv_spi_t spi = NRF_DRV_SPI_INSTANCE(SPI_INSTANCE);  /**< SPI instance. */
static volatile bool spi_xfer_done;  /**< Flag used to indicate that SPI instance completed the transfer. */
static volatile bool config_done = false;

static uint8_t m_tx_buf[] = BURST_READ;                     /**< TX buffer. */
static uint8_t m_rx_buf[sizeof(MOTION_registers) + 1];      /**< RX buffer. */
static const uint8_t m_tx_length = sizeof(m_tx_buf);        /**< Transfer length. */
static const uint8_t m_rx_length = sizeof(m_rx_buf);        /**< Receiver length. */

void spi_event_handler(nrf_drv_spi_evt_t const * p_event, void * p_context)
{
    spi_xfer_done = true;
    NRF_LOG_INFO("Transfer completed.");

    if (m_rx_buf[0] != 0)
    {
        NRF_LOG_INFO(" Received:");
        NRF_LOG_HEXDUMP_INFO(m_rx_buf, strlen((const char *)m_rx_buf));
    }
}


int main(void){
    
    bool verified = false;
    bsp_board_init(BSP_INIT_LEDS);

    APP_ERROR_CHECK(NRF_LOG_INIT(NULL));
    NRF_LOG_DEFAULT_BACKENDS_INIT();

    nrf_drv_spi_config_t spi_config = NRF_DRV_SPI_DEFAULT_CONFIG;
    spi_config.ss_pin   = SPI_SS_PIN;
    spi_config.miso_pin = SPI_MISO_PIN;
    spi_config.mosi_pin = SPI_MOSI_PIN;
    spi_config.sck_pin  = SPI_SCK_PIN;
    APP_ERROR_CHECK(nrf_drv_spi_init(&spi, &spi_config, spi_event_handler, NULL));

    NRF_LOG_INFO("SPI+Laser example started.");

    while(1){
        memset(m_rx_buf, 0, m_rx_length);
        spi_xfer_done = false;

      
        APP_ERROR_CHECK(nrf_drv_spi_transfer(&spi, m_tx_buf, m_tx_length, m_rx_buf, m_rx_length));

        while (!spi_xfer_done)
        {
            __WFE();
        }

        NRF_LOG_FLUSH();

        bsp_board_led_invert(BSP_BOARD_LED_0);
        nrf_delay_ms(1000);
    }
}

